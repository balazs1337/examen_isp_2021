package ex2;

import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;

public class InterfataGrafica  extends JFrame {

    private final String FILE_NAME = "file.txt";

    public InterfataGrafica() {

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setLayout(null);
        setSize(400,400);
        JTextArea fileArea= new JTextArea();
        fileArea.setBounds(20,40,360,20);
        JButton button = new JButton("Add text to file");
        button.setBounds(20,300,360,20);
        add(fileArea);
        add(button);
        button.addActionListener(e->{
            try{FileWriter fileWriter = new FileWriter(FILE_NAME);
                fileWriter.write(fileArea.getText());
            fileWriter.flush();}
            catch(IOException e1){
                System.out.println("Exception");
            }
            }
        );

    }
    public static void main(String[] args) throws IOException {

        new InterfataGrafica();



    }










}
