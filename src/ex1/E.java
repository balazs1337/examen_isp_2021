package ex1;

public class E {
    //asociere directa, declar un atribut de tipul clasei B
    B b;
    //pentru cazurile F si H, ele fiind prea abstracte, voi interpreta agregarea si compozitia ca fiind asocieri
    F f;
    H h;

    public void metG(int i) {
    }
}

//generalizare = mosternire, se face cu extends
class G extends B {
    public void i() {

    }
}

class D {

}

class B {

    private long t;

    public void x() {

        // Obiectul de tipul clasei d va fi folosit candva de catre metoda x-> dependenta
        D d = new D();// se va apela constructorul implicit


    }

}

class F {
    public void metA() {

    }
}

class H {

}

interface I {
//    E e;
}


